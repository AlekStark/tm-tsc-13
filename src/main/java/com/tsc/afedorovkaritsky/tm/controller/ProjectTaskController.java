package com.tsc.afedorovkaritsky.tm.controller;

import com.tsc.afedorovkaritsky.tm.api.controller.IProjectTaskController;
import com.tsc.afedorovkaritsky.tm.api.controller.ITaskController;
import com.tsc.afedorovkaritsky.tm.api.service.IProjectService;
import com.tsc.afedorovkaritsky.tm.api.service.IProjectTaskService;
import com.tsc.afedorovkaritsky.tm.api.service.ITaskService;
import com.tsc.afedorovkaritsky.tm.model.Project;
import com.tsc.afedorovkaritsky.tm.model.Task;
import com.tsc.afedorovkaritsky.tm.util.TerminalUtil;

import java.util.List;

public class ProjectTaskController implements IProjectTaskController {

    private final IProjectService projectService;
    private final ITaskService taskService;
    private final IProjectTaskService projectTaskService;
    private final ITaskController taskController;

    public ProjectTaskController(IProjectService projectService,
                                 ITaskService taskService,
                                 IProjectTaskService projectTaskService,
                                 ITaskController taskController) {
        this.projectService = projectService;
        this.taskService = taskService;
        this.projectTaskService = projectTaskService;
        this.taskController = taskController;
    }

    @Override
    public void bindTaskToProjectById() {
        System.out.println("[Введите Id проекта]");
        final String projectId = TerminalUtil.nextLine();
        final Project project = projectService.findProjectById(projectId);
        if (project == null) {
            System.out.println("Проект не найден");
            return;
        }
        System.out.println("[Введите Id задачи]");
        final String taskId = TerminalUtil.nextLine();
        final Task task = taskService.findTaskById(taskId);
        if (task == null) {
            System.out.println("Задача не найдена");
            return;
        }
        final Task taskUpdated = projectTaskService.bindTaskToProjectById(projectId, taskId);
        if (taskUpdated == null) System.out.println("Задача не обновлена");
        System.out.println("[OK]");
    }

    @Override
    public void unbindTaskFromProjectById() {
        System.out.println("[Введите Id проекта]");
        final String projectId = TerminalUtil.nextLine();
        final Project project = projectService.findProjectById(projectId);
        if (project == null) {
            System.out.println("Проект не найден");
            return;
        }
        System.out.println("[Введите Id задачи]");
        final String taskId = TerminalUtil.nextLine();
        final Task task = taskService.findTaskById(taskId);
        if (task == null) {
            System.out.println("Задача не найдена");
            return;
        }
        final Task taskUpdated = projectTaskService.unbindTaskFromProjectById(projectId, taskId);
        if (taskUpdated == null) System.out.println("Задача не обновлена");
        System.out.println("[OK]");
    }

    @Override
    public void findAllTasksByProjectId() {
        System.out.println("[Введите Id проекта]");
        final String projectId = TerminalUtil.nextLine();
        final Project project = projectService.findProjectById(projectId);
        if (project == null) {
            System.out.println("Проект не найден");
            return;
        }
        final List<Task> tasks = projectTaskService.findAllTasksByProjectId(projectId);
        if (tasks.size() <= 0) System.out.println("Нет задач привязаных к проекту");
        int index = 1;
        for (Task task : tasks) {
            System.out.println(index + ". " + task);
            index++;
        }
    }

    @Override
    public void removeProjectById() {
        System.out.println("[Введите Id проекта]");
        final String projectId = TerminalUtil.nextLine();
        if (projectId == null || projectId.isEmpty()) {
            System.out.println("Ошибка ввода");
        }
        projectTaskService.removeProjectById(projectId);
        System.out.println("[OK]");
    }

    @Override
    public void removeProjectByIndex() {
        System.out.println("[Введите индекс проекта]");
        final Integer projectIndex = TerminalUtil.nextNumber() - 1;
        if (projectIndex == null || projectIndex < 0) {
            System.out.println("Ошибка ввода");
        }
        projectTaskService.removeProjectByIndex(projectIndex);
        System.out.println("[OK]");
    }

    @Override
    public void removeProjectByName() {
        System.out.println("[Введите название проекта]");
        final String projectName = TerminalUtil.nextLine();
        if (projectName == null || projectName.isEmpty()) {
            System.out.println("Ошибка ввода");
        }
        projectTaskService.removeProjectByName(projectName);
    }

}