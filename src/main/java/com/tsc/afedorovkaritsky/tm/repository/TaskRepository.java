package com.tsc.afedorovkaritsky.tm.repository;

import com.tsc.afedorovkaritsky.tm.api.repository.ITaskRepository;
import com.tsc.afedorovkaritsky.tm.enumerated.Status;
import com.tsc.afedorovkaritsky.tm.model.Task;

import java.util.*;

public class TaskRepository implements ITaskRepository {

    private final List<Task> tasks = new ArrayList<>();

    @Override
    public void add(final Task task) {
        tasks.add(task);
    }

    @Override
    public void remove(final Task task) {
        tasks.remove(task);
    }

    @Override
    public List<Task> findAll() {
        return tasks;
    }

    @Override
    public void clear() {
        tasks.clear();
    }

    @Override
    public Task findTaskById(final String id) {
        for (Task task : tasks) {
            if (id.equals(task.getId())) return task;
        }
        return null;
    }

    @Override
    public Task findTaskByName(final String name) {
        for (Task task : tasks) {
            if (name.equals(task.getName())) return task;
        }
        return null;
    }

    @Override
    public Task findTaskByIndex(final Integer index) {
        return tasks.get(index);
    }

    @Override
    public Task removeTaskById(final String id) {
        final Task task = findTaskById(id);
        if (task == null) return null;
        tasks.remove(task);
        return task;
    }

    @Override
    public Task removeTaskByName(final String name) {
        final Task task = findTaskByName(name);
        if (task == null) return null;
        tasks.remove(task);
        return task;
    }

    @Override
    public Task removeTaskByIndex(final Integer index) {
        final Task task = findTaskByIndex(index);
        if (task == null) return null;
        tasks.remove(task);
        return task;
    }

    @Override
    public Task updateTaskById(final String id, final String name, final String description) {
        if (id == null || id.isEmpty()) return null;
        if (name == null || name.isEmpty()) return null;
        final Task task = findTaskById(id);
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task updateTaskByIndex(final Integer index, final String name, final String description) {
        if (index == null || index < 0) return null;
        if (name == null || name.isEmpty()) return null;
        final Task task = findTaskByIndex(index);
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task startTaskById(String id) {
        final Task task = findTaskById(id);
        if (id == null || id.isEmpty()) return null;
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task startTaskByName(String name) {
        final Task task = findTaskByName(name);
        if (name == null || name.isEmpty()) return null;
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task startTaskByIndex(Integer index) {
        final Task task = findTaskByIndex(index);
        if (index == null || index < 0) return null;
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task finishTaskById(String id) {
        final Task task = findTaskById(id);
        if (id == null || id.isEmpty()) return null;
        task.setStatus(Status.COMPLETED);
        return task;
    }

    @Override
    public Task finishTaskByName(String name) {
        final Task task = findTaskByName(name);
        if (name == null || name.isEmpty()) return null;
        task.setStatus(Status.COMPLETED);
        return task;
    }

    @Override
    public Task finishTaskByIndex(Integer index) {
        final Task task = findTaskByIndex(index);
        if (index == null || index < 0) return null;
        task.setStatus(Status.COMPLETED);
        return task;
    }

    @Override
    public Task changeStatusTaskByIndex(Integer index, Status status) {
        final Task task = findTaskByIndex(index);
        if (index == null || index < 0) return null;
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeStatusTaskByName(String name, Status status) {
        final Task task = findTaskByName(name);
        if (name == null || name.isEmpty()) return null;
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeStatusTaskById(String id, Status status) {
        final Task task = findTaskById(id);
        if (id == null || id.isEmpty()) return null;
        task.setStatus(status);
        return task;
    }

    public int getCount() {
        return tasks.size();
    }

    @Override
    public List<Task> findAllTasksByProjectId(String projectId) {
        List<Task> taskList = new ArrayList<>();
        for (Task task : tasks) {
            if (projectId.equals(task.getProjectId()))
                taskList.add(task);
        }
        return taskList;
    }

    @Override
    public void removeTasksByProjectId(String projectId) {
        List<Task> taskList = findAllTasksByProjectId(projectId);
        for (Task task : taskList) {
            tasks.remove(task);
        }
    }

    @Override
    public boolean existsById(String taskId) {
        return findTaskById(taskId) != null;
    }

    @Override
    public Task bindTaskToProjectById(String projectId, String taskId) {
        final Task task = findTaskById(taskId);
        task.setProjectId(projectId);
        return task;
    }

    @Override
    public Task unbindTaskFromProjectById(String projectId, String taskId) {
        final Task task = findTaskById(taskId);
        task.setProjectId(null);
        return task;
    }

}
